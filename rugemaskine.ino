/**
 * ****************************************************************************
 * 
 * * Rugemaskine
 * 
 * ****************************************************************************
 * By Michael Egtved Christensen 2019 ->
 * 
 * Overvågning af temperatur / Luftfugtighed med en DHT sensor
 * 
 * 
 * see https://github.com/esp8266/Arduino for setting up the Arduino enviroment
 * 

 * 
 * * Fremhæv
 * ! Advarsel
 * ? Spørgsmål
 * TODO: TODO
 * @param parameter
*/

#include <MecLogging.h>
#include <MecWifi.h>
#include <MecWebThing.h>

// Sennor bibliotek
#include "DHTesp.h"

/**
 * ****************************************************************************
 *  GLOBALS
 * ****************************************************************************
 */
//
// To make stuff run every minnute
//
float humidity;
float temperature;
//int intervalpublish = 10000;
int intervalmesaure = 3000;
int intervaltimerpublish = 0;
int intervaltimermesaure = 0;
int tal = 0;

#define DHTTYPE DHTesp::DHT22 // DHT 11
#define DHTPIN 0              // D3

DHTesp dht;

#define DispCLK D5
#define DispDTA D6

byte value[] = {
    B11000000, // 0
    B11111001, // 1
    B10100100, // 2
    B10110000, // 3
    B10011001, // 4
    B10010010, // 5
    B10000010, // 6
    B11111000, // 7
    B10000000, // 8
    B10010000, // 9
    B01000000, // 0
    B01111001, // 1
    B00100100, // 2
    B00110000, // 3
    B00011001, // 4
    B00010010, // 5
    B00000010, // 6
    B01111000, // 7
    B00000000, // 8
    B00010000, // 9
};             // display nothing

byte digit[] = {B00000001, // left segment
                B00000010,
                B00000100,
                B00001000,
                B00010000,
                B00100000,
                B01000000,
                B10000000}; // right segment

/**
 * ****************************************************************************
 *  Setting up Debug
 * ****************************************************************************
 */

// LED to flash when there is activity
#define ActivityLedPin D4
#define ActivityLedMode Inverse // None, Inverse or Normal
// Debug level
//    - Inf for "production"
//    - Deb for "debug/development"
//    see libary Debug.h for all the debug levels
#define MyLogLevel Inf

MecLogging MyLog(MyLogLevel, ActivityLedPin, ActivityLedMode);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */
String WiFiSSID[] = {"Clemens 5n",
                     "Clemens 5nn",
                     "Michaels Phone",
                     "Clemens-5G"};
String WiFiPassword[] = {"zurtkeld",
                         "zurtkeld",
                         "zurtkeld",
                         "zurtkeld"};
int WifiCount = 4;

String ThingID = "Rugemaskine";

MecWifi MyWifi(WiFiSSID, WiFiPassword, WifiCount, &MyLog, ThingID);

/**
 * ****************************************************************************
 *  Setting up Thing
 * ****************************************************************************
 */

String ThingType[] = {"TemperatureSensor",
                      "Thermostat"};
int ThingTypeCount = 2;

MecThing Thing(ThingID, "Rugemaskinen", "WebThing Rugemaskine", ThingType, ThingTypeCount, &MyLog, &MyWifi);
float DefaultValue = 99.01;

MecProperty TemperatureProperty(&MyLog, "TemperatureProperty", "Temperatur", "Temperaturen i rugemaskinen", DefaultValue, "TemperatureProperty", true);
MecProperty PropLuftfugtighed(&MyLog, "LuftfugtighedProperty", "Luftfugtighed", "Luftfugtigheden i rugemaskinen", DefaultValue, "", true);

////////////////////////////////////////////////////////////////////////////////
// Setup
//
////////////////////////////////////////////////////////////////////////////////
void setup()
{

  pinMode(ActivityLedPin, OUTPUT);
  // Init libaries
  MyLog.ActivityLED(LedON);
  MyLog.setup();
  MyWifi.setup();

  Thing.addProperty(&TemperatureProperty);
  Thing.addProperty(&PropLuftfugtighed);

  Thing.setup();
  MyLog.infoNl("Setup- start...", Deb);

  // Kode til startup her
  // Initialize device.
  dht.setup(DHTPIN, DHTTYPE); // Connect DHT sensor to GPIO 17

  pinMode(DispCLK, OUTPUT);
  pinMode(DispDTA, OUTPUT);

  // publish end setup
  MyLog.infoNl("Setup- End...", Deb);
  MyLog.ActivityLED(LedOFF);
}

////////////////////////////////////////////////////////////////////////////////
// Loop
//
////////////////////////////////////////////////////////////////////////////////
void loop()
{
  unsigned i;

  unsigned int pow[] = {10000000,
                        1000000,
                        100000,
                        10000,
                        1000,
                        100,
                        10,
                        1};

  // Loop libaries
  //MyLog.ActivityLED(LedON);
  //MyDebug.infonl("Loop- Start", Deb);
  MyWifi.loop();
  MyLog.loop();
  Thing.loop();

  if (intervaltimermesaure < millis())
  {
    //MyDebug.infonl("Sensor: Tid til ny måling", Inf);
    MyLog.ActivityLED(LedON);

    intervaltimermesaure = millis() + intervalmesaure;

    // Get temperature event and print its value.
    humidity = dht.getHumidity();
    temperature = dht.getTemperature();
    tal = humidity * 100 + 1000000 * temperature;
    TemperatureProperty.SetValue((float)temperature);
    PropLuftfugtighed.SetValue((float)humidity);
    // MyLog.infoNl("temperatur " + String(temperature), Inf);
    //MyDebug.infonl("Sensor: Måling slut", Inf);
  }

  /*
  if (intervaltimerpublish < millis())
  {
    MyLog.infoNl("Sensor: sender data", Inf);

    intervaltimerpublish = millis() + intervalpublish;

    TemperatureProperty.SetValue((float)temperature);
    PropLuftfugtighed.SetValue((float)20.2);
  }
  */

  for (int n = 0; n <= 7; n++)
  {
    i = (tal / pow[n]) % 10;
    shiftOut(DispDTA, DispCLK, MSBFIRST, B11111111); // select all segments
    shiftOut(DispDTA, DispCLK, MSBFIRST, B11111111); // display nothing
    delayMicroseconds(500);
    //Serial.println(i);
    if (n == 1 || n == 5)
    {
      i = i + 10;
    }
    //Serial.println(i);
    shiftOut(DispDTA, DispCLK, MSBFIRST, value[i]);
    shiftOut(DispDTA, DispCLK, MSBFIRST, digit[n]);
    delayMicroseconds(1000);
  }

  //MyDebug.infonl("Loop- Slut", Deb);
  MyLog.ActivityLED(LedOFF);
  //delay(20);
}
